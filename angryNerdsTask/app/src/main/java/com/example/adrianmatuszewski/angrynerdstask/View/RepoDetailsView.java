package com.example.adrianmatuszewski.angrynerdstask.View;


import com.example.adrianmatuszewski.angrynerdstask.Model.dataBaseModel.Repo;

import android.content.Context;

/**
 * @author: Adrian Matuszewski
 */
public interface RepoDetailsView extends BaseView  {

    Context getContext();

    void setChosenRepo(final Repo repo);


}
