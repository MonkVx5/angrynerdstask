package com.example.adrianmatuszewski.angrynerdstask.Model;


import com.example.adrianmatuszewski.angrynerdstask.Model.dataBaseModel.Repo;

import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public interface DataSource {

    void getRepos(GenericCallback<List<Repo>> callbackResponse);

    Repo getRepo(int i);

}
