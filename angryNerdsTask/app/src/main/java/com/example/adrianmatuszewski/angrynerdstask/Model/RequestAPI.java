package com.example.adrianmatuszewski.angrynerdstask.Model;


import com.example.adrianmatuszewski.angrynerdstask.Model.dataBaseModel.Repo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;

/**
 * @author: Adrian Matuszewski
 */
public interface RequestAPI {

    @GET("repos")
    Call<List<Repo>> getRepos();

}
