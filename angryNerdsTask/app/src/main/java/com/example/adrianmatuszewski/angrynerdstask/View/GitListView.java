package com.example.adrianmatuszewski.angrynerdstask.View;



import com.example.adrianmatuszewski.angrynerdstask.Model.dataBaseModel.Repo;

import android.content.Context;
import android.support.v7.widget.SearchView;

import java.util.List;

/**
 * @author: Adrian Matuszewski
 */
public interface GitListView extends BaseView{

    Context getContext();

    void setList(List<Repo> list);

    void showRepoDetails(final int chosenRepo);


}
