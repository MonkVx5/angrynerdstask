package com.example.adrianmatuszewski.angrynerdstask.Model;

/**
 * @author: Adrian Matuszewski
 */
public interface GenericCallback<T> {

    void onSuccess(T data);

    void onFailure(Exception error);
}
