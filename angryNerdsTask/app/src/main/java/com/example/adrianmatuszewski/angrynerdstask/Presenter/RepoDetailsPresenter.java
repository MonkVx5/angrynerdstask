package com.example.adrianmatuszewski.angrynerdstask.Presenter;


import com.example.adrianmatuszewski.angrynerdstask.Model.DataSource;
import com.example.adrianmatuszewski.angrynerdstask.Model.GenericCallback;
import com.example.adrianmatuszewski.angrynerdstask.Model.RetrofitDataSource;
import com.example.adrianmatuszewski.angrynerdstask.Model.dataBaseModel.Repo;
import com.example.adrianmatuszewski.angrynerdstask.View.RepoDetailsView;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * @author: Adrian Matuszewski
 */
public class RepoDetailsPresenter<T extends RepoDetailsView> extends Presenter<T>  {

    private int mId;

    private Repo mRepo;

    public RepoDetailsPresenter(final Context context) {}

    public void prepareChosenRepo(int id) {
        mId = id;
        mRepo = RetrofitDataSource.getInstance().getRepo(id);
        mView.setChosenRepo(mRepo);
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void finish() {

    }

}
